# Fetal Health


Setup
-----

```bash
$ git clone https://gitlab.com/1120_ed_13/fetal_health.git
```

Jupyter build and run
---------------------

##### Build
```bash
$ docker-compose build
```
##### Run (GPU)
```bash
$ docker-compose up
```
--------------------