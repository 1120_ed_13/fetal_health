############################################
#       Setup python environment
############################################
#FROM nvcr.io/nvidia/cuda:10.1-cudnn7-devel-ubuntu18.04
FROM python:3.7.5-slim

# root:users - should allow us to have access to files outside of container without needing root
USER 0:100
# Adds metadata to the image as a key value pair example LABEL version="1.0"
# LABEL version=$IMAGE_VERSION
# Set environment variables
ENV LANG=C.UTF-8 LC_ALL=C.UTF-8

# OS packages
RUN apt-get update && apt-get install -y --no-install-recommends \
    curl python3.7 python3.7-dev python3.7-distutils \
    build-essential -y \
    && apt-get autoremove -y \
    && apt-get clean \
    && rm -rf /tmp/* /var/tmp/* \
    && rm -rf /var/lib/apt/lists/*

# set python 3.7 as default
RUN update-alternatives --install /usr/bin/python python /usr/bin/python3.7 1 \
    && update-alternatives --install /usr/bin/python3 python3 /usr/bin/python3.7 1

# install pip
RUN curl https://bootstrap.pypa.io/get-pip.py -o /tmp/get-pip.py \
    && python /tmp/get-pip.py


############################################
#       Setup Jupyter environment
############################################
# OS packages
RUN apt-get update && apt-get install -y --no-install-recommends \
    wget git-core htop pkg-config unzip tree freetds-dev zsh vim \
    && apt-get autoremove -y \
    && apt-get clean \
    && rm -rf /tmp/* /var/tmp/* \
    && rm -rf /var/lib/apt/lists/*

# setup nodejs
RUN cd /root && wget https://nodejs.org/dist/v14.11.0/node-v14.11.0-linux-x64.tar.xz \
    && tar -xvf node-v14.11.0-linux-x64.tar.xz \
    && ln -s /root/node-v14.11.0-linux-x64/bin/node /usr/bin/node \
    && ln -s /root/node-v14.11.0-linux-x64/bin/npm /usr/bin/npm

# jupyter terminal with zsh
RUN sh -c "$(curl -fsSL https://raw.github.com/ohmyzsh/ohmyzsh/master/tools/install.sh)"
ENV SHELL=/usr/bin/zsh
RUN chsh root -s $SHELL

# install python packages
COPY docker_scripts/jupyter-requirements.txt /tmp
RUN pip install -r /tmp/jupyter-requirements.txt

# therminal dark theme
RUN mkdir -p ~/.jupyter/lab/user-settings/@jupyterlab/terminal-extension \
    && echo '{"theme": "dark"}' > ~/.jupyter/lab/user-settings/@jupyterlab/terminal-extension/plugin.jupyterlab-settings

# jupyter lab extensions
RUN jupyter-labextension install @jupyter-widgets/jupyterlab-manager
RUN jupyter-labextension install @aquirdturtle/collapsible_headings
RUN jupyter-labextension install @jupyterlab/toc
RUN jupyter-labextension install jupyterlab-topbar-extension
RUN jupyter-labextension install jupyterlab-system-monitor
RUN jupyter-labextension install jupyterlab-topbar-text
RUN jupyter-labextension install jupyterlab-logout
RUN jupyter-labextension install jupyterlab-theme-toggle


############################################
#       Setup Development environment
############################################

WORKDIR /root/workbench

# Fix notebooks-dev permissions
RUN chown 0:100 /root/workbench \
    && chmod g+rwx -R /root/workbench

# OS packages
RUN apt-get update && apt-get install -y --no-install-recommends \
    protobuf-compiler libtool automake libleptonica-dev\
    libgl1-mesa-glx \
    && apt-get autoremove -y \
    && apt-get clean \
    && rm -rf /tmp/* /var/tmp/* \
    && rm -rf /var/lib/apt/lists/*



# Copy pip.conf
RUN mkdir -p /root/.pip/
COPY docker_scripts/pip.conf /root/.pip/pip.conf

# install python packages
COPY requirements.txt /build-scripts/requirements.txt
RUN pip install -r /build-scripts/requirements.txt

# install setup.py python packages
COPY docker_scripts/setup_requirements.py /build-scripts/setup_requirements.py
RUN python /build-scripts/setup_requirements.py

COPY docker_scripts/entrypoint.sh /usr/bin/entrypoint.sh
ENTRYPOINT sh /usr/bin/entrypoint.sh

